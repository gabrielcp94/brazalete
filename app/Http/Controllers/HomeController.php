<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function escaner()
    {
        return view('escaner');
    }

    public function escanear(Request $request)
    {
        $tipoBusqueda = intval(substr($request->codigo, 0, 2));
        $id = intval(substr($request->codigo, 2));
        switch ($tipoBusqueda) {
            case 1:
                return redirect('http://10.4.237.27/fichaPacienteId?pac='.$id);
                break;
            
            default:
                # code...
                break;
        }
    }

    public function brazalete(Request $request)
    {
        $paciente = Paciente::find($request->id);
        return view('brazalete', compact('paciente'));
    }
}