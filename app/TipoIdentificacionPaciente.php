<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoIdentificacionPaciente extends Model
{
    protected $table = 'gen_tipo_identificacion_paciente';
}
