<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Brazaletes') }}</title>
    <!-- Scripts -->
    {{-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> --}}
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>
<style>
    @media print {
        @page {
            /* size: landscape */
        }
        .formulario{
            display: none;
        }
    }
    .rotate90 {
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }
</style>

<div class=" brazalete" style="border: 0px solid #A1A1A1;padding: 10px;width: 27.93cm; height: 1.05cm; display:flex; margin-left:0cm; " >
    <div style="margin-top:10px; margin-right:0px; display:contents">
        <img src="{{ url('') }}/img/logo.png" style="width:10%; height:130%" alt="">
    </div>
    <div class="col-md-12 brazaletePaciente" style="font-size:18px; width:8cm; margin-top:-38px; margin-left:5px;">
        <h5 class="nombreP" style="margin-bottom:2px;"> </h5>
        <div style="font-size:16px;">
            <span>{{$paciente->nombre}}</span>
            <br>
            <span>{{$paciente->tipoIdentificacion->tx_descripcion}}: {{$paciente->identificacion}}</span>
            <br>
            <span>FICHA: {{$paciente->nr_ficha}}</span>
            <br>
            <span>Edad: {{$paciente->edad}} ({{$paciente->fecha_nacimiento}})</span>
            <br>
            {{-- <span>F. ADMISIÓN: </span> --}}
        </div>
    </div>
</div>
<div class=" brazalete" style="border: 0px solid #A1A1A1;padding: 10px;width: 10.93cm; height: 0.45cm; display:flex; margin-left:0cm; " >
    <?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG("01".$paciente->id, "C39+",1.5,33) . '" alt="barcode"   />'; ?>
</div>
<script>
    print();
</script>