@extends('adminlte::page')

@section('title', 'Escanear Codigo de Barra')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
    <br>
    <div class="card card-info">
        <div class="card-header" style="font-size: 80px" align="center">
            Escanear Codigo de Barra
        </div>
        <div class="card-body">
            <input type="number" style="font-size: 80px; height:140px" class="form-control" id="codigo" name="codigo" autofocus onkeypress="escanear($(this).val());">
        </div>
    </div>
@stop

@section('js')
<script>
    $("body").addClass("sidebar-collapse");
    function escanear(codigo) {
        window.location.href = "escanear?codigo="+codigo;
    }
</script>
@stop